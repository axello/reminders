import Vapor

extension Droplet {
    func setupRoutes() throws {

        // response to requests to /info domain
        // with a description of the request
        get("info") { req in
            return req.description
        }

        // api
        let remindersController = RemindersController()
        remindersController.addRoutes(to: self)
        let usersController = UsersController()
        usersController.addRoutes(to: self)
        let categoriesController = CategoriesController()
        categoriesController.addRoutes(to: self)

        // web
        let webController = WebController(viewRenderer: view)
        webController.addRoutes(to: self)

    }
}
